set(ENABLE_python2 OFF CACHE BOOL "")
set(ENABLE_python3 ON CACHE BOOL "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_common.cmake")
