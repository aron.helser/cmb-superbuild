# Cubit is a private program to which Kitware developers have access. We provide
# logic for situating these programs into our superbuild for development
# purposes only. Nothing is bundled or distributed.
superbuild_add_project(cubit
  CAN_USE_SYSTEM
  CONFIGURE_COMMAND ""
  BUILD_COMMAND
    ${CMAKE_COMMAND} -E copy_directory "<SOURCE_DIR>" "<BINARY_DIR>"
  INSTALL_COMMAND ""
)

set(CUBIT_EXE ${CMAKE_CURRENT_BINARY_DIR}/cubit/build/Cubit)
