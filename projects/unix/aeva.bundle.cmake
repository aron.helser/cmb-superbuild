set(cmb_doc_dir "share/cmb/doc")
set(cmb_example_dir "share/examples")
set(plugin_dir "lib")

include(aeva.bundle.common)
include(cmb.bundle.unix)

# Install PDF guides.
cmb_install_extra_data()
