set(cmb_doc_dir "share/cmb/doc")
set(plugin_dir "lib")

include(modelbuilder.bundle.common)
set(cmb_package "modelbuilder ${cmb_version_major}.${cmb_version_minor}.${cmb_version_patch}")
set(cmb_doc_base_dir "Contents/doc")

include(cmb.bundle.apple)
