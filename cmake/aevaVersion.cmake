string(REPLACE "origin/" "" aeva_tag "${aeva_GIT_TAG}")
string(REPLACE ".git" "/raw/${aeva_tag}/version.txt" aeva_version_url "${aeva_GIT_REPOSITORY}")

# Download AEVA's master branch version.txt
file(DOWNLOAD "${aeva_version_url}"
  ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/aevaVersion.txt STATUS status)
list(GET status 0 error_code)
if (error_code)
  file(READ "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/aevaVersion.txt" output)
  message(FATAL_ERROR "Could not access the version file for AEVA: ${output}")
endif ()

file(STRINGS ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/aevaVersion.txt version_string )

string(REGEX MATCH "([0-9]+)\\.([0-9]+)\\.([0-9]+)[-]*(.*)"
  version_matches "${version_string}")

set(aeva_version_major ${CMAKE_MATCH_1})
set(aeva_version_minor ${CMAKE_MATCH_2})
set(aeva_version_patch "${CMAKE_MATCH_3}")
# Do we just have a patch version or are there extra stuff?
if (CMAKE_MATCH_4)
  set(aeva_version_patch "${CMAKE_MATCH_3}-${CMAKE_MATCH_4}")
endif()
set(aeva_version "${aeva_version_major}.${aeva_version_minor}")
